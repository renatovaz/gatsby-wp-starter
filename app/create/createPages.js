module.exports = async ({ actions, graphql, reporter }) => {
  return graphql(query, { limit: 1000 }).then(result => {
    if (result.errors) {
      throw result.errors
    }
    // Create pages
    result.data.cms.pages.nodes.forEach(page => {
      actions.createPage({
        path: `${page.uri}`,
        component: require.resolve(`../src/templates/page.js`),
        context: page,
      });
      reporter.info(`created page: ${page.uri}`);
    });
  });
};

const query = `
  query PagesQuery {
    cms {
      pages(where: { parent: null }) {
        nodes {
          id
          isFrontPage
          pageId
          slug
          title
          uri
        }
      }
    }
  }
`;
