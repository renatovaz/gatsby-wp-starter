const createPages = require(`./create/createPages`);
const createImages = require(`./create/createImages`);

exports.createPages = async ({ actions, graphql, reporter }) => {
  await createPages({ actions, graphql, reporter });
};

exports.createResolvers = ({ actions, cache, createNodeId, createResolvers, getNode, store, reporter }) => {
  createImages({ actions, cache, createNodeId, createResolvers, getNode, store, reporter });
};
