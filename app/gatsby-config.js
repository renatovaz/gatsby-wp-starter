require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}` || `.env`,
});

module.exports = {
  siteMetadata: {
    title: `Gatsby WP Starter`,
    description: `This is a barebones gatsby wordpress starter.`,
    author: `@weareredlight`,
    apiUrl: process.env.API_URL,
    siteUrl: process.env.SITE_URL,
    keywords: [`Gatsby, WordPress, GraphQL`],
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-resolve-src`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'src',
        path: `${__dirname}/src/`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-wp-starter`,
        short_name: `gatsby-wp`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#7035FD`,
        display: `minimal-ui`,
        icon: `src/assets/images/favicon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        policy: [{ userAgent: '*', allow: '/' }],
      }
    },
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        exclude: [`/admin`],
      }
    },
    {
      resolve: `gatsby-source-graphql`,
      options: {
        typeName: `WPGraphQL`,
        fieldName: `cms`,
        url: `${process.env.API_URL}/graphql`,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
