import { graphql } from 'gatsby';

export const query = graphql`
  fragment JumpTabFragment on WPGraphQL_Page_Pagebuilder_content_Blocs_JumpTab {
    __typename
    name
    target
  }
`;
