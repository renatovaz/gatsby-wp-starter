import { graphql } from 'gatsby';

export const query = graphql`
  fragment AccordionFragment on WPGraphQL_Page_Pagebuilder_content_Blocs_Accordion {
    __typename
    title
    hasText
    text
  }
`;
