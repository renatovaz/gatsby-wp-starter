import { graphql } from 'gatsby';

export const query = graphql`
  fragment PictureFragment on WPGraphQL_Page_Pagebuilder_content_Blocs_Picture {
    __typename
    imageAlt
    image {
      mediaItemUrl
      sourceUrl
      mediaItemId
      modified
      imageFile {
        childImageSharp {
          fluid(quality: 90, maxWidth: 2400) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  }
`;
