import { graphql } from 'gatsby';

export const query = graphql`
  fragment CallToActionFragment on WPGraphQL_Page_Pagebuilder_content_Blocs_CallToAction {
    __typename
    displayAsButton
    text
    external
    url
    target
    page {
      ... on WPGraphQL_Page {
        uri
      }
    }
  }
`;
