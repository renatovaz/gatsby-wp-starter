import { graphql } from 'gatsby';

export const query = graphql`
  fragment ContactFormFragment on WPGraphQL_Page_Pagebuilder_content_Blocs_ContactForm {
    __typename
    form
    formId
  }
`;
