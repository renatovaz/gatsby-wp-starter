import { graphql } from 'gatsby';

export const query = graphql`
  fragment CoverFragment on WPGraphQL_Page_Pagebuilder_content_Blocs_Cover {
    __typename
    className
    strapline
    title
    quote
    author
    showSearch
    showHelp
    showButton
    buttonText
    buttonTarget {
      ... on WPGraphQL_Page {
        uri
      }
    }
  }
`;
