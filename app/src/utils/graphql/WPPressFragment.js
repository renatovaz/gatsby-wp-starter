import { graphql } from 'gatsby';

export const query = graphql`
  fragment WPPressFragment on WPGraphQL_Press {
    press {
      title
      text
      textModal
      date
      newsType
      linkToVideo
      linkToExternal
      photo {
        mediaItemId
        mediaItemUrl
        modified
        sourceUrl
        imageFile {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      photoAlt
      tags {
        ... on WPGraphQL_Tag {
          name
        }
      }
    }
  }
`;
