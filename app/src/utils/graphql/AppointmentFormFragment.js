import { graphql } from 'gatsby';

export const query = graphql`
  fragment AppointmentFormFragment on WPGraphQL_SiteOptions {
    __typename
    siteOptions {
      offices {
        name
      }
      subjects {
        name
      }
    }
  }
`;
