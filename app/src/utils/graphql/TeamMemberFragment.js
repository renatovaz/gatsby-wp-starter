import { graphql } from 'gatsby';

export const query = graphql`
  fragment TeamMemberFragment on WPGraphQL_Page_Pagebuilder_content_Blocs_TeamMember {
    __typename
    specialty
    spotlight
    spotlightDescription
    teamMemberWrapper {
      ... on WPGraphQL_TeamMember {
        teamMember {
          name
          nameSpecialty
          photo {
            mediaItemId
            mediaItemUrl
            modified
            sourceUrl
            imageFile {
              childImageSharp {
                fluid(quality: 90, maxWidth: 800) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
          photoAlt
          profile
          role
          roleSpecialty
        }
      }
    }
  }
`;
