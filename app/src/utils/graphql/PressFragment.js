import { graphql } from 'gatsby';

export const query = graphql`
  fragment PressFragment on WPGraphQL_Page_Pagebuilder_content_Blocs_Press {
    __typename
    spotlight
    pressWrapper {
      ... on WPGraphQL_Press {
        ...WPPressFragment
      }
    }
  }
`;
