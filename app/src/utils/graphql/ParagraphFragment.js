import { graphql } from 'gatsby';

export const query = graphql`
  fragment ParagraphFragment on WPGraphQL_Page_Pagebuilder_content_Blocs_Paragraph {
    __typename
    align
    text
  }
`;
