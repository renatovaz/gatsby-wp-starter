import { graphql } from 'gatsby';

export const query = graphql`
  fragment OfficeFragment on WPGraphQL_Page_Pagebuilder_content_Blocs_Office {
    __typename
    concise
    officeWrapper {
      ... on WPGraphQL_Office {
        office {
          address
          mapLink
          name
          phone
          gallery {
            mediaItemId
            mediaItemUrl
            modified
            sourceUrl
            imageFile {
              childImageSharp {
                fluid(quality: 90, maxWidth: 800) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`;
