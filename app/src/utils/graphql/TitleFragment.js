import { graphql } from 'gatsby';

export const query = graphql`
  fragment TitleFragment on WPGraphQL_Page_Pagebuilder_content_Blocs_Title {
    __typename
    align
    level
    text
  }
`;
