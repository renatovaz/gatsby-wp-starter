import { graphql } from 'gatsby';

export const query = graphql`
  fragment PressGroupFragment on WPGraphQL_Page_Pagebuilder_content_Blocs_PressGroup {
    __typename
    news {
      spotlight
      pressWrapper {
        ... on WPGraphQL_Press {
          ...WPPressFragment
        }
      }
    }
  }
`;
