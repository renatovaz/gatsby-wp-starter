export const routes = {
  'home': { to: '/', external: false, newTab: false },
  'privacy': { to: '/privacy-policy/', external: false, newTab: false },
};
