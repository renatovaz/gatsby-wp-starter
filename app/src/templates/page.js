import React, { Fragment } from 'react';
import { graphql } from 'gatsby';
import { Layout, SEO } from 'components';
import { builder } from 'utils/blocs';

export default ({ data }) => {
  const { title, seoWidget, pageBuilder } = data.cms.page;

  return (
    <Layout>
      <SEO title={title} seoWidget={seoWidget} />
      {
        pageBuilder.content && pageBuilder.content.map(({ className, targetId, blocs }, idx) => (
          <div key={idx} id={targetId} className={className}>
            {blocs.map((bloc, blocId) => (
              <Fragment key={blocId}>
                {builder(bloc)}
              </Fragment>
            ))}
          </div>
        ))
      }
    </Layout>
  );
};

export const pageQuery = graphql`
  query PageQuery($id: ID!) {
    cms {
      page(id: $id) {
        isFrontPage
        title
        seoWidget {
          title
          description
          keywords
          socialImage {
            mediaItemUrl
          }
        }
        pageBuilder {
          content {
            className
            targetId
            blocs {
              ...AccordionFragment
              ...CallToActionFragment
              ...ContactFormFragment
              ...CoverFragment
              ...JumpTabFragment
              ...OfficeFragment
              ...ParagraphFragment
              ...PictureFragment
              ...PressFragment
              ...PressGroupFragment
              ...TeamMemberFragment
              ...TitleFragment
            }
          }
        }
      }
    }
  }
`;
