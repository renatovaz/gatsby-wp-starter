import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import { Link } from 'components';

const Cover = ({ bloc, className }) => {
  const [searching, setSearching] = useState(false);
  const [results, setResults] = useState(-1);
  const [keyword, setKeyword] = useState('');
  const { author, buttonTarget, buttonText, quote, showButton, showHelp, showSearch, strapline, title } = bloc;
  const msgNoResults = results === 0 && { 'data-no-results': 'Não foram encontrados resultados para a sua pesquisa.' };

  const clearSearch = () => {
    setSearching(false);
    setKeyword('');
    document.documentElement.classList.remove('is-searching');
    document.documentElement.classList.remove('no-results');
  };

  const handleInput = (word) => {
    setKeyword(word);
  }

  const handleSearch = (ev) => {
    ev.stopPropagation();
    setSearching(true);
    document.documentElement.classList.add('is-searching');
    // handle word highlight and total results
    const totalResults = document.querySelectorAll('mark').length;
    (totalResults === 0) ? document.documentElement.classList.add('no-results') : document.documentElement.classList.remove('no-results');
    setResults(totalResults);
  };

  useEffect(() => {
    return () => {
      document.documentElement.classList.remove('is-searching');
      document.documentElement.classList.remove('no-results');
    };
  }, []);

  return (
    <div className={cx('cover-wrapper fvw', [className], [bloc.className])} {...msgNoResults}>
      <header>
        {strapline && <p className='strapline'>{strapline}</p>}
        <h1 dangerouslySetInnerHTML={{ __html: title }} />
        {quote &&
          <blockquote>
            {quote}
            {author && <footer>- {author}</footer>}
          </blockquote>
        }
        {showButton && <Link to={buttonTarget.uri} className='button'>{buttonText}</Link>}
        {showSearch &&
          <div className='input-wrapper input-search'>
            <input type='search' name='keyword' placeholder='Procure por palavra chave...' value={keyword} onChange={(ev) => handleInput(ev.target.value)} />
            {keyword.length > 0 &&
              <>
                <button className='btn-search' onClick={handleSearch}>pesquisar</button>
                <button className='btn-clear-search' onClick={clearSearch} type='reset' />
              </>
            }
            {searching && <p className='small mt-10'>{results} {`Resultado${results !== 1 ? 's' : ''}`}</p>}
          </div>
        }
        {showHelp &&
          <Link to='#contacts-sos' className='btn-help'>Precisa de ajuda?</Link>
        }
      </header>
    </div>
  );
};

Cover.propTypes = {
  bloc: PropTypes.object.isRequired,
  className: PropTypes.string,
};

Cover.defaultProps = {
  className: '',
};

export default Cover;
