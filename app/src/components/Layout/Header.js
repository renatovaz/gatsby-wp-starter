import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useStaticQuery, graphql } from 'gatsby';
import Helmet from 'react-helmet';
import CookieConsent from 'react-cookie-consent';
import Cookies from 'js-cookie';
import cx from 'classnames';

import Menu from './Menu';

const Header = ({ className }) => {
  const data = useStaticQuery(query);
  const cookieName = 'StarterCookie';
  const [hasConsent, setConsent] = useState(Cookies.get(cookieName) || false);
  const tracking = data.cms.analytics.siteOptions;

  const addGTAG = () => {
    return (
      <Helmet>
        {/* Global site tag (gtag.js) - Google Analytics */}
        <script async src={`https://www.googletagmanager.com/gtag/js?id=${tracking.googleAnalyticsId}`}></script>
        <script>
          {`
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${tracking.googleAnalyticsId}');
          `}
        </script>
      </Helmet>
    );
  };

  return (
    <header className={cx('header-menu', [className])}>
      {hasConsent && addGTAG()}
      <CookieConsent
        disableStyles
        location='bottom'
        buttonText='Compreendo'
        cookieName={cookieName}
        containerClasses='cookie-consent-wrapper'
        buttonClasses='btn-consent'
        expires={150}
        onAccept={() => setConsent(true)}
      >
        {tracking.consentMessage}
      </CookieConsent>
      <Menu />
    </header>
  );
};

Header.propTypes = {
  className: PropTypes.string,
};

Header.defaultProps = {
  className: '',
};

export default Header;

const query = graphql`
  query AnalyticsQuery {
    cms {
      analytics: siteOptions {
        siteOptions {
          googleAnalyticsId
          consentMessage
        }
      }
    }
  }
`;
