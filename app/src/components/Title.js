import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Title = ({ bloc, className }) => {
  const { align, level, text } = bloc;
  const Element = level;

  return (
    <div className={cx('title-wrapper', [className])}>
      <Element className={`t-${align}`}>{text}</Element>
    </div>
  );
};

Title.propTypes = {
  bloc: PropTypes.object.isRequired,
  className: PropTypes.string,
};

Title.defaultProps = {
  className: '',
};

export default Title;
