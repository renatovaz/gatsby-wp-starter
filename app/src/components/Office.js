import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import { Image, Link, Slideshow } from 'components';

const Office = ({ bloc, className }) => {
  const { concise, officeWrapper: { office } } = bloc;
  const { name, phone, address, mapLink, gallery } = office;

  if (concise) {
    return (
      <div className={cx('office-wrapper', [className])}>
        <div className='info-wrapper'>
          <h4 className='office-name'>{name}</h4>
          {phone &&
            <Link to={`tel:${phone.replace(/\s+/g, '')}`} external newTab className='office-phone'>
              <Image image={'icons/icon_phone.svg'} />
              {phone}
            </Link>
          }
        </div>
      </div>
    );
  }

  return (
    <div className={cx('office-wrapper', [className])}>
      <Slideshow className='gallery-wrapper'>
        {gallery.map((image, idx) => <Image key={idx} image={image} />)}
      </Slideshow>
      <div className='info-wrapper'>
        <div>
          <h4 className='office-name'>{name}</h4>
          {phone &&
            <Link to={`tel:${phone.replace(/\s+/g, '')}`} external newTab className='office-phone'>
              <Image image={'icons/icon_phone.svg'} />
              {phone}
            </Link>
          }
        </div>
        <div className='office-address'>
          <p dangerouslySetInnerHTML={{ __html: address }} />
          {mapLink &&
            <Link to={mapLink} external newTab className='button'>ver mapa</Link>
          }
        </div>
      </div>
    </div>
  );
};

Office.propTypes = {
  bloc: PropTypes.object.isRequired,
  className: PropTypes.string,
};

Office.defaultProps = {
  className: '',
};

export default Office;
