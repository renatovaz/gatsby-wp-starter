import React from 'react';
import PropTypes from 'prop-types';
import { useStaticQuery, graphql } from 'gatsby';

import { Input } from 'components';

const FormFull = ({ className }) => {
  const data = useStaticQuery(query);
  const offices = data.cms.appointment.siteOptions.offices;
  const subjects = data.cms.appointment.siteOptions.subjects;
  const optionsOffices = offices.map(({ name }) => ({ value: name, label: name }));
  const optionsSubjects = subjects.map(({ name }) => ({ value: name, label: name }));

  return (
    <div className={className}>
      <div className='form-cols'>
        <Input id='appt-first-name' name='appt-first-name' label='Primeiro Nome: *' placeholder='escreva o seu primeiro nome' type='text' required />
        <Input id='appt-last-name' name='appt-last-name' label='Último Nome: *' placeholder='escreva o seu último nome' type='text' required />
      </div>
      <div className='form-cols'>
        <Input id='appt-email' name='appt-email' label='Email: *' placeholder='escreva o seu email' type='email' required />
        <Input id='appt-phone' name='appt-phone' label='Telefone:' placeholder='escreva o seu telefone' type='tel' />
      </div>
      <Input id='appt-specialty' name='appt-specialty' label='Especialidade: *' placeholder='procurar por especialidade' type='dropdown' options={optionsSubjects} required />
      <div className='form-cols'>
        <Input id='appt-city' name='appt-city' label='Cidade:' placeholder='procurar por cidade' type='dropdown' options={optionsOffices} />
        <Input id='appt-date' name='appt-date' label='Data:' placeholder='dd-mm-aaaa' type='text' />
      </div>
      <Input id='appt-obs' name='appt-obs' label='Observações:' placeholder='escreva aqui as suas observações' type='textarea' />
    </div>
  );
};

FormFull.propTypes = {
  className: PropTypes.string.isRequired,
};

FormFull.defaultProps = {
  className: '',
};

export default FormFull;

const query = graphql`
  query FormFullQuery {
    cms {
      appointment: siteOptions {
        ...AppointmentFormFragment
      }
    }
  }
`;
