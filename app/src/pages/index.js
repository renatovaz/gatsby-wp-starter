import React from 'react';
import { Image, Layout, SEO } from 'components';

const IndexPage = () => (
  <Layout>
    <SEO title='Home' />
    <Image image='favicon.png' style={{ maxWidth: '200px' }} />
    <h1>Welcome!</h1>
    <p>This is your Gatsby WordPress CMS starter.</p>
    <p>Delete this page once you start generating pages from WordPress.</p>
  </Layout>
);

export default IndexPage;
