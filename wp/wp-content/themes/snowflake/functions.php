<?php

// Add a new toolbar called "Minimalist"
function minimalist_toolbar($toolbars) {
  $toolbars['Minimalist'] = array();
  $toolbars['Minimalist'][1] = array('bold' , 'italic' , 'underline', 'strikethrough', 'link', 'fullscreen');

  return $toolbars;
}
add_filter('acf/fields/wysiwyg/toolbars', 'minimalist_toolbar');


// Add Menu and Localization Support
if (function_exists('add_theme_support')) {
  add_theme_support('menus');
  load_theme_textdomain('snowflake', get_template_directory().'/languages');
}


// Add Site Options page
if (function_exists('acf_add_options_page')) {
  acf_add_options_page(array(
    'page_title' => 'Site Options',
    'menu_title' => 'Site Options',
    'menu_slug' => 'site-options',
    'capability' => 'edit_posts',
    'redirect' => false,
    'show_in_graphql' => true,
  ));
}


// Add Custom Post Types
function add_custom_pages() {
  $office = (object)["name" => "office", "label" => "Offices", "single_name" => "Office", "plural_name" => "Offices", "icon" => "dashicons-building"];
  $team_member = (object)["name" => "team_member", "label" => "Team Members", "single_name" => "TeamMember", "plural_name" => "TeamMembers", "icon" => "dashicons-id"];
  $press = (object)["name" => "press", "label" => "Press", "single_name" => "Press", "plural_name" => "Press", "icon" => "dashicons-megaphone"];
  $custom_pages = array($office, $team_member, $press);

  foreach ($custom_pages as $custom_page) {
    register_post_type($custom_page->name, [
      'public' => true,
      'label' => $custom_page->label,
      'show_in_graphql' => true,
      'graphql_single_name' => $custom_page->single_name,
      'graphql_plural_name' => $custom_page->plural_name,
      'menu_icon' => $custom_page->icon,
    ]);
  }
}
add_action('init', 'add_custom_pages');


// Disable Gutenberg editor
add_filter('use_block_editor_for_post', '__return_false', 10);


// Remove revisions, tinymce and comments from pages and posts
function remove_pages_editor() {
  remove_post_type_support('post', 'revisions');
  remove_post_type_support('post', 'comments');
  remove_post_type_support('page', 'editor');
  remove_post_type_support('page', 'revisions');
  remove_post_type_support('office', 'editor');
  remove_post_type_support('team_member', 'editor');
  remove_post_type_support('press', 'editor');
}
add_action('init', 'remove_pages_editor');


// Add custom styles to our Page Builder
function custom_acf_repeater_colors() {
  echo
  '<style type="text/css">
  /* add border to the repeater block row */
  .acf-repeater.-block > table > tbody > tr > td {
    border-top-width: 10px;
    border-color: #f9f9f9!important;
  }
  /* exclude the forist row */
  .acf-repeater.-block > table > tbody > tr:first-child > td {
    border-top-width: 0px;
  }
  /* in case if you got any nested repeater blocks and want to hide the border */
  .acf-repeater.-block > table > tbody > tr > td .acf-repeater.-block > table > tbody > tr > td {
    border-top-width: 0px;
    border-top-color: #e1e1e1;
  }
  /* stronger background color on the sides */
  .acf-repeater .acf-row-handle.order,
  .acf-repeater .acf-row-handle.remove {
    background: #f1f1f1;
  }
  </style>';
}
add_action('admin_head', 'custom_acf_repeater_colors');

?>
