# SNOWFLAKE

Empty WordPress theme. This WP instance is to be used as a headless cms.


## Base plugins
* Adminimize
* Advanced Custom Fields Pro
* Akismet anti-spam
* _breadcrumb navxt_
* Contact Form 7
* Flamingo (dev only to test form submissions)
* JAMstack Deployments
* _polylang_
* Simple Page Ordering
* Imagemagick Engine
* Wordfence Security
* WP GraphiQL
* WP GraphQL
* WPGraphQL for Advanced Custom Fields


## Custom

SEO is a custom block on each page
Analytics is a custom field on Site Options page
Site Options handle default information about SEO and one time configs
