<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Snowflake theme</title>

  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <main id="main" class="content-wrapper" role="main">
    <h1>Snowflake Theme!</h1>
  </main>

  <?php wp_footer(); ?>

</body>
</html>
